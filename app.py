from flask import Flask, request, render_template, render_template_string
import pandas as pd
import joblib
import os
import bs4
#import getpass
import datetime
from waitress import serve
import sys
import sqlite3
sys.path.append('./static')
from updater import upload_data

app = Flask(__name__,static_folder='static',
            template_folder='templates')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

@app.route("/",methods=['GET','POST'])
def index():
    # Pull data from database
    conn=sqlite3.connect("static/xrv.db",detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
    cur=conn.cursor()
    df = pd.read_sql_query("select * from Data",conn)
    df.to_csv("static/data_all.csv")
    conn.close()
    
    if request.method == "POST":
        
        
        fp=request.form.get("myFile")
        for i in os.listdir(fp):
            if i[-4:]=='xlsm':
                fp = os.path.join(fp,i)
                break
        gantry=request.form.get("gantry")
        direction = request.form.get("direction")

        if direction=='CW_36Angles':
            direction = 'CW'
            angles = list(range(190,360,10)) + list(range(0,190,10))
            update_result = upload_data(fp,direction,gantry,angles=angles)
        elif direction == 'CCW_36Angles':
            print(direction)
            direction = 'CCW'
            angles = list(range(175,0,-10)) + list(range(355,175,-10))
            update_result = upload_data(fp,direction,gantry,angles=angles)
        else:
            update_result = upload_data(fp,direction,gantry)

        if not update_result[0]:
            #Need to generate a popup with error message
            pass

    return render_template("xrv.html")

   
    
if __name__ == '__main__':
    #app.run(host="10.6.37.81",port=610,debug=True)
    serve(app, host="10.6.37.81", port=610)   