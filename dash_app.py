import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State
import plotly.express as px
import pandas as pd
from waitress import serve
import plotly.graph_objs as go  # Import Plotly graph objects
import os

# Import Flask-Session
from flask_session import Session

# Existing imports
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import re

# Import Flask
from flask import Flask, session

# Initialize your Flask app
server = Flask(__name__)

# Configure your Flask app for Flask-Session
server.config['SESSION_TYPE'] = 'filesystem'  

with open('config.txt','r') as f:
    key = f.read()
server.config['SECRET_KEY'] = key  

# Initialize Session with your Flask app
Session(server)


class xrv:
    def __init__(self, fp, direction = 'CW',gantry=None,energies = [220,200,180,165,145,120,110,100,90,70],angles = [225,270, 315, 0, 45, 90, 135, 180]):
        date = pd.read_excel(fp,sheet_name='Source',header=None).iloc[0,3]
        if not gantry:
            gantry = re.search("(?i)gtr[1-3]",fp.replace(' ','')).group()[-1]
        self.target = {'x':0, 'y':0, 'z':145}  # X, Y, Z
        
        if direction=='CW':
            pass
        else:
            angles = angles[::-1]
        
        num_entries = int(len(energies)*len(angles))
        self.all_beams = pd.read_excel(fp, sheet_name='All Beams',header=None).iloc[:num_entries,11:14]
        self.all_beams.columns = ['x','y','z']
        self.all_beams['datetime'] = pd.to_datetime(date)
        self.all_beams['gantry'] = gantry
        self.all_beams['energy']= len(angles)*energies
        self.all_beams['angle']= np.ravel([len(energies)*[x] for x in angles])
        for i in ['x','y','z']:
            self.all_beams[f'd{i}']=self.all_beams[i] - self.target[i]
        self.all_beams['delta'] = self.all_beams.apply(lambda x: np.sqrt(x['dx']**2 + x['dy']**2 + x['dz']**2), axis=1)
        self.all_beams = self.all_beams.astype({i:float for i in ['x','y','z','dx','dy','dz']})
        

# Assuming 'df' is your DataFrame
# Example DataFrame
#df = pd.read_csv(r"C:\Users\mau22560.AD\Desktop\Python Scripts\2023\xrv\static\data_all.csv")

app = dash.Dash(__name__, server = server)


app.layout = html.Div([
    html.Div([
        html.Div([
            html.H3('Folder containing .xlsm', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),  # Title for the file path input
            dcc.Input(
                id='input-filepath',
                type='text',
                placeholder='Enter file path here...',
                style={'width': '400px', 'display': 'inline-block'}
            )
        ], style={'display': 'flex', 'alignItems': 'center','marginBottom': '2px'}),
        html.Div([
            html.H3('Gantry Selection', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),
            dcc.RadioItems(
                id='gantry-radio',
                options=[
                    {'label': 'GTR1', 'value': 'GTR1'},
                    {'label': 'GTR2', 'value': 'GTR2'},
                    {'label': 'GTR3', 'value': 'GTR3'}
                ],
                value='GTR1',  # Default value
                labelStyle={'display': 'inline-block'}
            )
        ], style={'display': 'flex', 'alignItems': 'center','marginBottom': '2px'}),
        html.Div([
            html.H3('Direction Selection', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),
            dcc.RadioItems(
                id='direction-radio',
                options=[
                    {'label': 'CW', 'value': 'CW'},
                    {'label': 'CCW', 'value': 'CCW'}
                ],
                value='CW',  # Default value
                labelStyle={'display': 'inline-block'}
            )
        ], style={'display': 'flex', 'alignItems': 'center','marginBottom': '2px'}),
        html.Div([
            html.H3('Angle Selection', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),
            dcc.RadioItems(  # RadioItems for selecting the number of angles
                id='angle-radio',
                options=[
                    {'label': '8 Angles (monthly)', 'value': '8'},
                    {'label': '36 Angles (every 10 degrees, starting at 190 for CW or 175 for CCW)', 'value': '36'}
                ],
                value='36',  # Default value
                labelStyle={'display': 'inline-block'}
            )
        ], style={'display': 'flex', 'alignItems': 'center','marginBottom': '2px'}),
        html.Div([
                html.Button('Load', id='submit-val', n_clicks=0, style={'fontSize': '20px', 'padding': '10px 20px'}),
                html.H5('Status: ', style={'display': 'inline-block', 'padding': '10px 20px'},id='container-button-basic'),
            ], style={'padding': '5px'})], style={'marginBottom': 5,'background-color':'grey'}),
    html.Div([
        html.H3('Graph Type Selection', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),
        dcc.Dropdown(
            id='graph-type-dropdown',
            options=[
                {'label': 'Bar Graph', 'value': 'bar'},
                {'label': 'Box Plot', 'value': 'box'}
            ],
            value=None,  # Default value
            style={'width': '300px', 'display': 'inline-block'}
        )
    ], style={'display': 'flex', 'alignItems': 'center'}),
    html.Div([
        html.H3('Data Selection', style={'margin':'5px','marginRight': '10px', 'display': 'inline-block'}),
        dcc.RadioItems(  # RadioItems for selecting the data to show
            id='data-radio',
            options=[
                {'label': 'Delta', 'value': 'delta'},
                {'label': 'DX', 'value': 'dx'},
                {'label': 'DY', 'value': 'dy'},
                {'label': 'DZ', 'value': 'dz'}
            ],
            value='delta',  # Default value
            labelStyle={'display': 'inline-block'}
        )
    ], style={'display': 'flex', 'alignItems': 'center'}),
    dcc.Graph(id='graph-output'),
    html.Iframe(src='http://10.6.37.81:610', width="100%", height="1080px"),
], style = {'overflowY': 'scroll'})


@app.callback(
    [Output('container-button-basic', 'children'),
    Output('graph-output', 'figure',allow_duplicate=True)],
    [Input('submit-val', 'n_clicks')],
    [State('input-filepath', 'value'),
     State('direction-radio', 'value'),
     State('gantry-radio', 'value'),
     State('angle-radio', 'value')],
     prevent_initial_call=True)

def update_output(n_clicks, fp, direction, gantry, angles):
    empty_figure = go.Figure()
    if n_clicks > 0:
        try:
            filename = [x for x in os.listdir(fp) if x[-4:] == 'xlsm'][0]
            fp = os.path.join(fp,filename)
            if angles == '36':
                if direction == 'CW':
                    angles = list(range(190,360,10)) + list(range(0,190,10))
                elif direction == 'CCW':
                    angles = list(range(175,0,-10)) + list(range(355,175,-10))
                df = xrv(fp,direction,gantry,angles=angles).all_beams
            else:
                df = xrv(fp,direction,gantry).all_beams 
            
            session['data'] = df.to_json(date_format='iso', orient='split')  # Store in session
            return f'Loaded {filename} with {len(df)} rows.', empty_figure
        except Exception as e:
            return f'Error loading file: {e}', empty_figure
    return 'Enter a file path and click submit.', empty_figure

@app.callback(
    Output('graph-output', 'figure',allow_duplicate=True),
    [Input('graph-type-dropdown', 'value'),
    Input('data-radio', 'value')],prevent_initial_call=True
)
def update_graph(graph_type,data_type):
    df = pd.read_json(session['data'], orient='split').sort_values(by='energy').astype({'energy':str})
    viridis = ['rgb(0.283072, 0.130895, 0.449241)', 'rgb(0.262138, 0.242286, 0.520837)', 'rgb(0.220057, 0.343307, 0.549413)', 'rgb(0.177423, 0.437527, 0.557565)', 'rgb(0.143343, 0.522773, 0.556295)', 'rgb(0.119512, 0.607464, 0.540218)', 'rgb(0.166383, 0.690856, 0.496502)', 'rgb(0.319809, 0.770914, 0.411152)', 'rgb(0.525776, 0.833491, 0.288127)', 'rgb(0.762373, 0.876424, 0.137064)']

    if graph_type == 'bar':
        fig = px.bar(df, x='angle', y=data_type, color='energy', color_discrete_sequence=viridis,
                     labels={data_type: data_type.capitalize() + ' [mm]', 'angle': 'Angle'}, 
                     title=f'Bar Graph of {data_type.capitalize()} by Angle and Energy')
        fig.update_layout(barmode='group')
    elif graph_type == 'box':
        fig = px.box(df, x='angle', y=data_type,
                     labels={data_type: data_type.capitalize() +' [mm]', 'angle': 'Angle'}, 
                     title=f'Box Plot of {data_type.capitalize()} by Angle')
    return fig



if __name__ == '__main__':
    #app.run_server(debug=False)
    serve(app.server, port=611)