# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 09:31:54 2024

@author: mau22560
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
#from xrv import xrv


class xrv_radial_plot:
    def __init__(self,df):
        self.df = self.add_plot_angle(df)
        self.df = self.df.sort_values(by = ['angle','energy']).reset_index(drop=True)
        self.df = pd.concat([self.df, pd.DataFrame([self.df.iloc[0,:]])], ignore_index=True)
        #self.df = self.df.append(self.df.iloc[0,:],ignore_index=True)
    def add_plot_angle(self,df,buffer = 20):
        ''' returns the angles for each point taking into account the buffer angle between each gantry angle and evenly spacing energies '''
        n_angle,n_energy = df.nunique()[['angle','energy']]
        space_per_angle = (360-n_angle*buffer)/n_angle
        space_per_energy = space_per_angle/(n_energy-1)
        sorted_energy = sorted(np.unique(df['energy']))
        dict_energy_angle = {y:x for x,y in zip(range(n_energy),sorted_energy)}
        
        def get_angle(angle,energy):
            center_angle = angle
            start_angle = angle - space_per_angle/2
            return start_angle + dict_energy_angle[energy]*space_per_energy
        
        df['plot_angle'] = df.apply(lambda x:get_angle(x['angle'],x['energy'])*np.pi/180,axis=1)
        return df
    def plot(self,fig):
        ax = fig.add_subplot(111, projection='polar')
        
        min_val = np.min(np.min(self.df[['dx','dy','dz','delta']]))
        max_val = np.max(np.max(self.df[['dx','dy','dz','delta']]))
        
        r_min = -1.5
        r_max = 1.5
        
        if min_val<r_min:
            r_min = min_val-.5
        if max_val>r_max:
            r_max = max_val+.5
            
        ax.set_rorigin(r_min-1)
        ax.set_ylim(r_min,r_max)
        
        ax.set_theta_zero_location('N')
        # Set the theta direction to clockwise
        ax.set_theta_direction(-1)
        #ax.set_rticks([-1,0,1])
       
        
        
        
        
        
        ax.plot(self.df['plot_angle'],self.df['dx'],'o-',label = 'ΔX',color='orange',alpha=.5)
        ax.plot(self.df['plot_angle'],self.df['dy'],'o-',label = 'ΔY',color='green',alpha=.5)
        ax.plot(self.df['plot_angle'],self.df['dz'],'o-',label = 'ΔZ',color='blue',alpha=.5)
        ax.plot(self.df['plot_angle'],self.df['delta'],'o',label = 'Δ',color='k')
        ax.legend(loc='center',bbox_to_anchor=(0.5,0.5))
        
        
        # Set the radial grid lines
        rgrid_lines = ax.set_rgrids([-1, 0, 1])[0]
        ax.relim()
        ax.autoscale_view()
        
        # Get all the y-grid lines
        lines = ax.yaxis.get_gridlines()
        
        # Iterate over each line
        for idx,line in enumerate(lines):
            # Check if the gridline corresponds to -1 or 1
            if idx in [0,2]:
                # Change the color of the grid line
                line.set_color('red')
        
        # Disable the outer ring
        ax.spines['polar'].set_visible(False)
                
        ax.grid(linewidth=2, alpha = .3)
        #Radmachine save
        return fig

class xrv_distribution_plots:
    def __init__(self,xrv_detailed_results):
        self.data = xrv_detailed_results
    def plot_distribution(self,xrv_detailed_results,ax,par='dx',legend=True,pal=None):
        dx = xrv_detailed_results[par]
        Energies=[]
        Values = []
        for idx,val in dx.items():
            Energies.append(float(idx.split(',')[1].split(')')[0]))
            Values.append(val)
        
        if not pal:
            pal = sns.cubehelix_palette(start=0, rot=-.75,n_colors=len(np.unique(Energies)),light=.7,dark=.05)

        aa_df = pd.DataFrame(data={"Energy (MeV)":Energies,par:Values})
        sns.kdeplot(x=par,hue='Energy (MeV)',data=aa_df,ax=ax,legend=legend,palette=pal)
        if par!='delta':
            ax.set_title(par.replace('d','Δ').upper())
        elif par == 'delta':
            ax.set_title('Δ')
    def xrv_dist_plots(self,fig,ax,pal=None):
        #fig,ax = plt.subplots(4,1,sharex=True,figsize=(9,8))
        fig.suptitle("Distribution Plots (KDE) per parameter")
        for idx,par in enumerate(['dx','dy','dz','delta']):
            if idx==0:
                self.plot_distribution(self.data,ax[idx],par=par,legend=True,pal=pal)
            else:
                self.plot_distribution(self.data,ax[idx],par=par,legend=False,pal=pal)
        sns.move_legend(ax[0], "upper left", bbox_to_anchor=(1, 1))
        ax[3].set_xlabel("Value [mm]")
        fig.subplots_adjust(top=0.88,
        bottom=0.11,
        left=0.125,
        right=0.865,
        hspace=0.2,
        wspace=0.2)
    

