var all_files;

// Load the CSV data
var svgWidth = 800;
var svgHeight = 800;

var innerR = 100;
var outerR = svgWidth/2 - 50;
var all_Data;
var energies = [220,200,180,165,145,120,110,100,90,70];
energies = energies.reverse()
var angles = [225,270, 315, 0, 45, 90, 135, 180];
var dateToFileMapping={};
var datetime_string;
var file_id;
var rscale_min = -1.5;
var rscale_max = 1.5;

var spread = d => [d.dx,d.dy,d.dz];
var z_color = function(d){
if (Math.abs(d.dz)<.5){
  return "rgba(0,0,0,.9)";
} else {
  return d3.scaleLinear().domain([-.5,.5]).range(['blue','red']).clamp(true)(d.dz)}
}

var dx_coord = d => d.dx;
var dy_coord = d=> d.dy;
var dz_coord = d=> d.dz;
var dr_coord = d=> d.delta;

var lineX;
var lineY;
var lineZ;

var radius_map;
var angle_map;
var available;
var angle_energy;
var current_file;
var current_data;
var pad_energy = 10;

var mouseData;

var table = d3.select('body').append('table')
                .style("border-collapse", "collapse")
                .style("border", "2px black solid");
table.append("thead")
      .append("tr")
      .selectAll("th")
      .data(["ANGLE","MEAN (mm)","STD (mm)","MAX (mm)"])
      .enter()
      .append("th")
      .text((d)=>d)
      .style("border", "1px black solid")
      .style("padding", "5px")
      .style("background-color", "lightgray")
      .style("font-weight", "bold");

      

d3.csv("./static/data_all.csv?t="+Date.now()).then(function(data) {

  var parseTime = d3.timeParse("%Y-%m-%d %H:%M:%S");
  var dateToFileMapping={};

  // Parse the data
  data.forEach(function(d) {
    d.x = +d.x
    d.y = +d.y
    d.z = +d.z
    d.dx = +d.dx
    d.dy = +d.dy
    d.dz = +d.dz
    d.delta = +d.delta
    d.angle = +d.angle;
    d.energy = +d.energy;
    d.datetime = parseTime(d.datetime);
    d.gantry = +d.gantry
  });
  all_Data = data;
  var availableDates = [...new Set(all_Data.map((item) => {
                    aa = item.datetime;
            return aa.getDate().toString()+"-"+(aa.getMonth()+1).toString()+'-'+aa.getFullYear();}))]


  data.map(function(i){
    var month = i.datetime.getMonth() +1;
    if(month<10){
      month = "0" + month.toString();
    } else {
      month = month.toString();
    }
    var day = i.datetime.getDate();
    if(day<10){
      day = "0" + day.toString();
    } else {
      day = day.toString();
    }
    var year = i.datetime.getFullYear().toString();

    datetime_string = month+"/"+day+"/"+year;
    
    file_id = i.datetime.toLocaleString() + " GTR= " + i.gantry.toString();
    
    if (!(datetime_string in dateToFileMapping)){
      dateToFileMapping[datetime_string]=[file_id];

    } else if(!(dateToFileMapping[datetime_string].includes(file_id))) {
      dateToFileMapping[datetime_string].push(file_id);
    };
  });

    $( function() {

  available = function(date) {
    dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
    if ($.inArray(dmy, availableDates) != -1) {
      return [true, "","Available"];
    } else {
      return [false,"","unAvailable"];
    }
  }
  $('#datepicker').datepicker({ 
    beforeShowDay: available,
    onSelect: function(dateText) {
      d3.selectAll("option#fileList").remove()
      var files = dateToFileMapping[dateText];
      if (files) {
        $('#fileList').empty();
        files.forEach(function(file) {
          $('#fileList').append($('<option>').text(file));
        });
      }
    }
  });
  } ) ;
  // Create a scale for the radial lines
  radius_map = d3.scaleLinear()
    .domain([-1.5, 1.5]) //d3.max(data, function(d) { return d.delta; })])
    .range([innerR, outerR]);

  // Create a scale for the angles
  angle_map = d3.scaleLinear()
    .domain([0, 180])
    .range([Math.PI/2, -1/2*Math.PI]);

  // Create scale functino for each energy
  angle_energy = function(angle,energy,pad_theta){
    var delta_angle = 360/angles.length; // amount of angular displacement for each angle
    var delta_angle_energy = (delta_angle - 2 * pad_theta)/(energies.length-1); // angular displacement between energy points. 

    var answer = angle - delta_angle/2 + pad_theta + delta_angle_energy * energies.indexOf(energy)
    if (answer<0) { answer = answer + 360}
    return answer 

  }

  // Update to accomodate 360 points
  angle_energy = function(angle,energy,pad_theta){
    var delta_angle = 360/angles.length; // amount of angular displacement for each angle
    var delta_angle_energy = (delta_angle)/(energies.length-1); // angular displacement between energy points. 

    var answer = angle // - delta_angle/2 + pad_theta + delta_angle_energy * energies.indexOf(energy)
    if (answer<0) { answer = answer + 360}
    return answer 

  }

  // line generators 
  lineX =d3.line()
            .x(function(d){
               return radius_map(d.dx)*math.cos(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            .y(function(d){
               return -1*radius_map(d.dx)*math.sin(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            ;//.curve(d3.curveNatural);

  lineY =d3.line()
            .x(function(d){
               return radius_map(d.dy)*math.cos(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            .y(function(d){
               return -1*radius_map(d.dy)*math.sin(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            ;//.curve(d3.curveNatural);

  lineZ =d3.line()
            .x(function(d){
               return radius_map(d.dz)*math.cos(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            .y(function(d){
               return -1*radius_map(d.dz)*math.sin(angle_map(angle_energy(d.angle,d.energy,pad_energy)))})
            ;//.curve(d3.curveNatural);


  let winx = window.innerWidth;
  let winy = window.innerHeight;
  let tableX = Number(d3.format("d")(
                          document.querySelector("table")
                                  .getBoundingClientRect().right));
  let input_areaY = Number(d3.format("d")(
                              document.getElementById("input_area")
                                      .getBoundingClientRect().bottom));
  var svgX;
  if ((winx-svgWidth)/2>tableX){
    svgX = (winx-svgWidth)/2;
  } else if ((winx-tableX)/2>tableX) {
    svgX = (winx-tableX)/2;
  } else {
    svgX = tableX;
  }

  let svgY = input_areaY+10;//(winy-svgHeight)/2+20;
  // Create an SVG container
  var svg = d3.select("body").append("svg")
    .attr("width", svgWidth)
    .attr("height", svgHeight)
    .style("top",svgY+"px")
    .style("left",svgX+"px");
    // .attr("transform", "translate("+svgX+","+svgY+")");
  // Add backgroung rectangel for export purposes

  svg.append('rect')
  .attr('x', 0)
  .attr('y', 0)
  .attr('width', svgWidth)
  .attr('height', svgHeight)
  .attr('stroke', 'none')
  .attr('fill', 'white')
  .style("z-index",-1000);

  // Create a group for the plot and move it to the center of the SVG
  var plot = svg.append("g").attr("id","plot_area")
    .attr("transform", "translate("+svgWidth/2+","+svgHeight/2+")");

  var circle_components = plot.append("g").attr("class","circle_components");
  var color_radial_lines = d3.scaleLinear().domain([-1,0,1]).range(['red','black','red'])
  var legend = d3.select("#plot_area")
          .append('g')
          .attr("id","legend_box")
          .classed("hidden",false);


        var legend_x = legend.append("g")
          .attr("class","legend x");
          legend_x.append("line")
          .attr("y1",-10)//-1*svgHeight/2+10)
          .attr("x1",-5)//svgWidth/2-50)
          .attr("y2",-10)//-1*svgHeight/2+10)
          .attr("x2",5)//svgWidth/2-40)
          .attr("stroke-width",3)
          .style("stroke",'rgb(252,141,98)');
          legend_x.append("text")
          
          .attr("y",-10)//-1*svgHeight/2+10)
          .attr("x",10)//svgWidth/2-35)
          .attr("alignment-baseline","middle")
          .text("\u0394X");
        var legend_y = legend.append("g")
          .attr("class","legend y");
        legend_y.append("line")
          .attr("y1",5)//-1*svgHeight/2+25)
          .attr("x1",-5)//svgWidth/2-50)
          .attr("y2",5)//-1*svgHeight/2+25)
          .attr("x2",5)//svgWidth/2-40)
          .attr("stroke-width",3)
          .style("stroke",'rgb(102,194,165)');
        legend_y.append("text")
          .attr("y",5)//-1*svgHeight/2+25)
          .attr("x",10)//svgWidth/2-35)
          .attr("alignment-baseline","middle")
          .text("\u0394Y");
        var legend_z = legend.append("g")
          .attr("class","legend z");
        legend_z.append("line")
          .attr("y1",20)//-1*svgHeight/2+40)
          .attr("x1",-5)//svgWidth/2-50)
          .attr("y2",20)//-1*svgHeight/2+40)
          .attr("x2",5)//svgWidth/2-40)
          .attr("stroke-width",3)
          .style("stroke",'rgb(141,160,203)');
        legend_z.append("text")
          .attr("y",20)//-1*svgHeight/2+40)
          .attr("x",10)//svgWidth/2-35)
          .attr("alignment-baseline","middle")
          .text("\u0394Z");

  d3.select("g")
      .append("g")
      .attr('class','radial_lines')
      .attr("text-anchor","middle")
      .selectAll()
      .data([-1,0,1].reverse())
      .join("g")
  .call(g => g.append("circle")
      .attr("class","tolerance_circle")
      .attr("fill", "none")
      .attr("stroke", (d)=>{return color_radial_lines(d);})
      .attr("stroke-width",4)
      .attr("stroke-opacity", 0.1)
      .attr("r", radius_map)
      )// .attr("stroke-dasharray","5,5"))
  .call(g => g.append("text")
      .attr("class","tolerance_text")
      .attr("y", d => -radius_map(d))
      .attr("dy", "0.35em")
      .attr("stroke", "none")
      .attr("stroke-width", 5)
      .attr("fill", "currentColor")
      .attr("paint-order", "stroke")
      //.style("background-color",'rgba(200,200,200,.5)')
      .text((x, i) => `${x}${i ? "" : "mm"}`)
    .clone(true)
      .attr("y", d => radius_map(d)));


  // var angle_labels = Array.from({ length:8}, (_,i) => [0 + 45*i, outerR+15])
  d3.select("g")
      .append("g")
      .attr('class','radial_lines')
      .attr("text-anchor","middle")
      .selectAll()
      .data(angles)
      .join("g")
  //.call(g => g.append("line")
      //.attr("stroke-width",.5)
      //.attr("stroke-opacity", 0.2)
      //.attr("x1", d => innerR*math.cos(angle_map(d)))
      //.attr("y1", d => innerR*math.sin(math.PI +angle_map(d)))
      //.attr("x2", d => outerR*math.cos(angle_map(d)))
      //.attr("y2", d => outerR*math.sin(math.PI + angle_map(d)))
      //.attr("stroke", "black")
     // )// .attr("stroke-dasharray","5,5"))
  .call(g => g.append("text")
      .attr("class","angle_text")
      .attr("x", d => outerR*math.cos(angle_map(d)))
      .attr("y", d => outerR*math.sin(angle_map(d)+math.PI))
      .attr("dy", "0.35em")
      .attr("stroke", "none")
      .attr("stroke-width", 5)
      .attr("fill", "currentColor")
      .attr("paint-order", "stroke")
      //.style("background-color",'rgba(200,200,200,.5)')
      .text(d => `${d}\u00B0`))

  plot.append("g")
          .attr("id","circle_data")
          .selectAll("circle")
          .data(d3.range(80))
          .enter()
          .append("circle")
          .attr("r", 3)
          .attr("class","data_circles")
          .style("fill","none");

        
  d3.selectAll("circle.data_circles")
    .on("mouseover",function(event,d){
        
        mouseData = d3.pointer(event);

  
        let svg_coord = document.querySelector("svg").getBoundingClientRect();
        let x =  svg_coord.left + svgWidth/2 + mouseData[0];
        let y = svg_coord.top + svgHeight/2 + mouseData[1];
 
        d3.select("#tooltipTitle").text("Angle: "+d.angle+'\u00B0')
        d3.select("#tooltipTitle2").text("Energy: "+d.energy+" MeV")
        d3.select("#tooltip")
          .style("left", x + "px")
          .style("top", y + "px")
          .select("#value")
          .text("Deviation: "+d3.format(".2f")(d.delta));
        d3.select("#dx")
          .text(d3.format(".2f")(d.dx));
        d3.select("#dy")
          .text(d3.format(".2f")(d.dy));
        d3.select("#dz")
          .text(d3.format(".2f")(d.dz));
        d3.select("#tooltip").classed("hidden", false);
        d3.select("#moreText").classed("hidden",false);})
      
      .on("mouseout", function() {
                //Hide the tooltip
                circle_components.selectAll("circle.components")
                  .remove();
                d3.select("#tooltip").classed("hidden", true);
                d3.select(this).classed("hidden", false);
                })

      

  d3.select('#update')
    .on("click",()=>{
      d3.selectAll(".line_components").remove();
      current_file = d3.select("#fileList").property('value')
      if (current_file!=""){
      current_data = all_Data.filter(function(i){
                            return current_file == i.datetime.toLocaleString() + " GTR= " + i.gantry.toString();});

      current_data = current_data.sort(function(a, b) {
                              if (a.angle === b.angle) {
                                return a.energy - b.energy;
                              }
                              return a.angle - b.angle;
                            });
      

      //Update radius_map if current_data exceeds current scale
      var x_min = d3.min(current_data,(d)=>d.dx)
      var y_min = d3.min(current_data,(d)=>d.dy)
      var z_min = d3.min(current_data,(d)=>d.dz)
      

      var x_max = d3.max(current_data,(d)=>d.dx)
      var y_max = d3.max(current_data,(d)=>d.dy)
      var z_max = d3.max(current_data,(d)=>d.dz)
      var delta_max = d3.max(current_data,(d)=>d.delta)

      var max_change = d3.max([x_max,y_max,z_max,delta_max])
      var min_change = d3.min([x_min,y_min,z_min])

      if (min_change<rscale_min){
        if (max_change>rscale_max){
          radius_map.domain([min_change,max_change]);
        } else {
          radisum_map.domain([min_change,rscale_max]);
        }
      }
      else if (max_change>rscale_max){
        radius_map.domain([rscale_min,max_change]);
      }
      else {
        radius_map.domain([rscale_min,rscale_max]);
      };

      //Update axis-circles
      d3.selectAll(".tolerance_circle")
        .transition()
        .duration(1000)
        .attr("r",(d)=>radius_map(d));
      //Update text on axis-circles
      d3.selectAll(".tolerance_text")
        .transition()
        .duration(1000)
        .attr("y",function(d){
          let y_coord = d3.select(this).attr("y");
          if (y_coord>0){

            return radius_map(d);
          }
          else {
            return -1*radius_map(d);
          }
        });


      

        var update = plot.selectAll("circle.data_circles").data(current_data);
        console.log(current_data);

        var enter = update.enter().append("circle")
                .attr("class", "data_circles")
                .attr("cx", 0)
                .attr("cy", 0)
                //.style("fill", z_color) // Uncomment if z_color is defined and needed
                .attr("r", 1);

        // Merge the enter and update selections
        var merged = enter.merge(update);

        merged.transition()
            .duration(1000)
            .attr("cx", function (d) { return radius_map(d.delta) * Math.cos(angle_map(angle_energy(d.angle, d.energy, pad_energy))); })
            .attr("cy", function (d) { return radius_map(d.delta) * Math.sin(Math.PI + angle_map(angle_energy(d.angle, d.energy, pad_energy))); })
            .attr("r", 5)
            .style("fill", 'None')
            .style("stroke", 'black') // Set the line color to black
            .style("stroke-width", '3'); // Set the line width (adjust as needed); // Or use z_color if applicable

        // Correctly target the exit selection for elements to be removed
        var exit = update.exit();
        exit.transition()
            .duration(1000)
            .attr("r", 10) // Optional: Increase the radius before disappearing
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", 0)
            .remove();

            d3.selectAll("circle.data_circles")
            .on("mouseover",function(event,d){
                
                mouseData = d3.pointer(event);
        
          
                let svg_coord = document.querySelector("svg").getBoundingClientRect();
                let x =  svg_coord.left + svgWidth/2 + mouseData[0];
                let y = svg_coord.top + svgHeight/2 + mouseData[1];
         
                d3.select("#tooltipTitle").text("Angle: "+d.angle+'\u00B0')
                d3.select("#tooltipTitle2").text("Energy: "+d.energy+" MeV")
                d3.select("#tooltip")
                  .style("left", x + "px")
                  .style("top", y + "px")
                  .select("#value")
                  .text("Deviation: "+d3.format(".2f")(d.delta));
                d3.select("#dx")
                  .text(d3.format(".2f")(d.dx));
                d3.select("#dy")
                  .text(d3.format(".2f")(d.dy));
                d3.select("#dz")
                  .text(d3.format(".2f")(d.dz));
                d3.select("#tooltip").classed("hidden", false);
                d3.select("#moreText").classed("hidden",false);})
              
              .on("mouseout", function() {
                        //Hide the tooltip
                        circle_components.selectAll("circle.components")
                          .remove();
                        d3.select("#tooltip").classed("hidden", true);
                        d3.select(this).classed("hidden", false);
                        })

      // Populate table
      d3.select("tbody").remove();
      var global_mean =d3.format(".3f")(d3.mean(current_data,d=>d.delta));
      var global_std = d3.format(".3f")(d3.deviation(current_data,d=>d.delta));
      var global_max = d3.format(".3f")(d3.max(current_data,d=> math.abs(d.delta)));

      var global_dx_mean = d3.format(".3f")(d3.mean(current_data,d=>d.dx));
      var global_dy_mean = d3.format(".3f")(d3.mean(current_data,d=>d.dy));
      var global_dz_mean = d3.format(".3f")(d3.mean(current_data,d=>d.dz));
      
      var global_dx_std = d3.format(".3f")(d3.deviation(current_data,d=>d.dx));
      var global_dy_std = d3.format(".3f")(d3.deviation(current_data,d=>d.dy));
      var global_dz_std = d3.format(".3f")(d3.deviation(current_data,d=>d.dz));
      
      var global_dx_max = d3.format(".3f")(d3.max(current_data,d=> math.abs(d.dx)));
      var global_dy_max = d3.format(".3f")(d3.max(current_data,d=> math.abs(d.dy)));
      var global_dz_max = d3.format(".3f")(d3.max(current_data,d=> math.abs(d.dz)));

      var data_by_angle = d3.group(current_data,(d)=>d.angle)
      var table_body = table.append("tbody");
      for (let angle of data_by_angle.keys()){
          let angle_data = data_by_angle.get(angle);
          let m = d3.mean(angle_data,d=>d.delta);
          let std = d3.deviation(angle_data,d=>d.delta);
          let maxd = d3.max(angle_data,d=> math.abs(d.delta));
          var tr = table_body.append("tr");
          tr.selectAll("td").data([angle,m,std,maxd])
          .enter().append("td")
          .text(function(d){return d3.format(".3f")(d);})
          .style("border", "1px black solid")
          .style("padding", "5px")
          .on("mouseover", function(){
          d3.select(this).style("background-color", "powderblue");
        })
          .on("mouseout", function(){
          d3.select(this).style("background-color", "white");
        })
          .style("font-size", "12px");
      };

      // var foot = table_body.append("tr");

      table_body.selectAll("anything") // Select rows, even if none exist yet
          .data([
              ["GLOBAL_Delta", global_mean, global_std, global_max],
              ["Global_DX", global_dx_mean, global_dx_std, global_dx_max],
              ["Global_DY", global_dy_mean, global_dy_std, global_dy_max],
              ["Global_DZ", global_dz_mean, global_dz_std, global_dz_max]
          ])
          .enter() // For each new data element, enter the selection
          .append("tr") // Append a row for each data element
          .selectAll("td")
          .data(d => d) // Bind the inner array to the cells
          .enter()
          .append("td") // Append a cell for each element in the inner array
          .text(d => d) // Set the text of each cell to its data value
          .style("border", "2px black solid")
          .style("padding", "5px")
          .style("background-color", "lightgray")
          .style("font-weight", "bold");
      


      current_data.push(current_data[0]);

      d3.select("#plot_area")
        .append('g')
        .attr("class","line_components x")
        .classed("hidden",true)
        .append("path")
        .datum(current_data)
        .attr("d",lineX)
        // .transition()
        // .duration(1000)
        .attr("stroke",'rgb(252,141,98)')
        .attr("fill","none")
        .attr("stroke-width",2)
        ;

      d3.select("#plot_area")
          .append('g')
          .attr("class","line_components y")
          .classed("hidden",true)
          .append("path")
          .datum(current_data)
          .attr("d",lineY)
          // .transition()
          // .duration(1000)
          .attr("stroke",'rgb(102,194,165)')
          .attr("fill","none")
          .attr("stroke-width",2)
          ;

      d3.select("#plot_area")
          .append('g')
          .attr("class","line_components z")
          .classed("hidden",true)
          .append("path")
          .datum(current_data)
          .attr("d",lineZ)
          // .transition()
          // .duration(1000)
          .attr("stroke",'rgb(141,160,203)')
          .attr("fill","none")
          .attr("stroke-width",2)
          ;
        
        current_data.pop();
        
        

        d3.selectAll(".legend")
          .on("click",function(event){
            if(this.classList.contains("x")) {
              d3.select(".line_components.x").classed("hidden",!d3.select(".line_components.x").classed("hidden"));}
            else if(this.classList.contains("y")) {
              d3.select(".line_components.y").classed("hidden",!d3.select(".line_components.y").classed("hidden"));}
            else if(this.classList.contains("z")) {
              d3.select(".line_components.z").classed("hidden",!d3.select(".line_components.z").classed("hidden"));}
            }
          );

        


        }} );
    d3.select("#export_files")
      .on("click",()=>{
          d3.selectAll(".line_components").classed("hidden",true);

          var svgElement = document.querySelector('svg');
          
          var svgString = new XMLSerializer().serializeToString(svgElement); // Convert SVG to string

          var canvas = document.createElement('canvas'); // Create a canvas
          var ctx = canvas.getContext('2d'); // Get the 2D context

          canvg.Canvg.fromString(ctx, svgString).start(); // Render the SVG on the canvas

          var img = canvas.toDataURL("image/png"); // Get the data URL for the PNG image

          // Create a link and click it to download the PNG
          var link = document.createElement('a');
          link.href = img;
          link.download = 'xrv_visualization' + new Date().toLocaleDateString()+ '.png';
          link.click();
            
          
          // Save csv
          // Select rows from table_id
          var separator = ','
          var rows = document.querySelectorAll('table tr');
          // Construct csv
          var csv = [];
          for (var i = 0; i < rows.length-1; i++) {
              var row = [], cols = rows[i].querySelectorAll('td, th');
              for (var j = 0; j < cols.length; j++) {
                  // Clean innertext to remove multiple spaces and jumpline (break csv)
                  var data = cols[j].innerText.replace(/(\r\n|\n|\r)/gm, '').replace(/(\s\s)/gm, ' ')
                  // Escape double-quote with double-double-quote 
                  data = data.replace(/"/g, '""');
                  // Push escaped string
                  row.push('"' + data + '"');
              }
              csv.push(row.join(separator));
          }
          var csv_string = csv.join('\n');
          console.log(csv_string);
          // Download it
          var filename = 'table_xrv_' + new Date().toLocaleDateString() + '.csv';
          var link = document.createElement('a');
          link.style.display = 'none';
          link.setAttribute('target', '_blank');
          link.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv_string));
          link.setAttribute('download', filename);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
            
          

    });
});
    /*
    d3.select('#toggle_xyz')
    .on("click",()=>{
      if (d3.select('#toggle_xyz').text().includes("Show")){
        d3.select("#legend_box").classed("hidden",false);
        d3.selectAll(".line_components").classed("hidden",false);
        d3.select("#toggle_xyz").text(d3.select('#toggle_xyz').text().replace("Show","Remove"));
    } else {
        d3.select("#legend_box").classed("hidden",true);
        d3.selectAll(".line_components").classed("hidden",true);
        d3.select("#toggle_xyz").text(d3.select('#toggle_xyz').text().replace("Remove","Show"));
    }
        });
    */