import sqlite3
import numpy as np
import pandas as pd
from xrv import xrv

def upload_data(fp,direction,gantry,angles=None):
    conn=sqlite3.connect("static/xrv.db",detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
    cur=conn.cursor()
    result = None
    try:
        if angles:
            df = xrv(fp,direction,gantry,angles=angles).all_beams
        else:
            df = xrv(fp,direction,gantry).all_beams
        df= df[['datetime','gantry','energy','angle','x','y','z','dx','dy','dz','delta']]
        all_data=[]
        for i,d in df.iterrows():
            xx = tuple([str(i) for i in list(d)])
            all_data.append(xx)

        cur.executemany("INSERT INTO Data VALUES(?,?,?,?,?,?,?,?,?,?,?);", all_data)
        conn.commit()
        result =  (True,1)
    except Exception as e:
        print(e)
        result = (False,e)

    if result[0]:
        df = pd.read_sql_query("select * from Data",conn)
        df.to_csv("static/data_all.csv")
        conn.close()
        return result
    else:
        return result
