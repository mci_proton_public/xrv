# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 19:17:44 2023

@author: mau22560
"""

import pandas as pd
#import matplotlib.pyplot as plt
import numpy as np
#import seaborn as sns
#import os
import re
import numpy as np


class xrv:
    def __init__(self, fp, direction = 'CW',gantry=None,energies = [220,200,180,165,145,120,110,100,90,70],angles = [225,270, 315, 0, 45, 90, 135, 180]):
        date = pd.read_excel(fp,sheet_name='Source',header=None).iloc[0,3]
        if not gantry:
            gantry = re.search("(?i)gtr[1-3]",fp.replace(' ','')).group()[-1]
        self.target = {'x':0, 'y':0, 'z':145}  # X, Y, Z
        
        if direction=='CW':
            pass
        else:
            angles = angles[::-1]
        
        num_entries = int(len(energies)*len(angles))
        self.all_beams = pd.read_excel(fp, sheet_name='All Beams',header=None).iloc[:num_entries,11:14]
        self.all_beams.columns = ['x','y','z']
        self.all_beams['datetime'] = pd.to_datetime(date)
        self.all_beams['gantry'] = gantry
        self.all_beams['energy']= len(angles)*energies
        self.all_beams['angle']= np.ravel([len(energies)*[x] for x in angles])
        for i in ['x','y','z']:
            self.all_beams[f'd{i}']=self.all_beams[i] - self.target[i]
        self.all_beams['delta'] = self.all_beams.apply(lambda x: np.sqrt(x['dx']**2 + x['dy']**2 + x['dz']**2), axis=1)
        self.all_beams = self.all_beams.astype({i:float for i in ['x','y','z','dx','dy','dz']})
        
        
    def calc_summary(self):
        df_sum = {}
        for angle,dfi in self.all_beams.groupby('angle'):
            maxi = float(dfi.loc[np.abs(dfi['delta'])==np.max(np.abs(dfi['delta']))]['delta'])
            mean = dfi.mean(numeric_only=True)['delta']
            std = dfi.std(numeric_only=True)['delta']
            df_sum[angle] = {"MEAN (mm)":mean,"STD (mm)":std,"MAX (mm)":maxi}

        maxi = float(self.all_beams.loc[np.abs(self.all_beams['delta'])==np.max(np.abs(self.all_beams['delta']))]['delta'])
        mean = self.all_beams.mean(numeric_only=True)['delta']
        std = self.all_beams.std(numeric_only=True)['delta']
        df_sum["GLOBAL"] = {"MEAN (mm)":mean,"STD (mm)":std,"MAX (mm)":maxi}
        df_sum = pd.DataFrame(df_sum).T
        return df_sum.round(3).to_dict()
    
    def get_radmachine_results(self):
        df_interested = self.all_beams[['dx','dy','dz','delta']]
        dx,dy,dz,ddelta = df_interested.abs().mean()
        sig_dx,sig_dy,sig_dz,sig_delta = df_interested.std()
        max_dx,max_dy,max_dz,max_delta = df_interested.abs().max()
        return {"summary":dict(self.calc_summary()),
                #"isofocus":self.get_avg_isofocus(),
                "avg":{"dx":dx,"dy":dy,"dz":dz,"delta":ddelta},
                "std":{"dx":sig_dx,"dy":sig_dy,"dz":sig_dz,"delta":sig_delta},
                "max":{"dx":max_dx,"dy":max_dy,"dz":max_dz,"delta":max_delta}}
        
    
    
