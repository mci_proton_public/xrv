
import sqlite3
import pandas as pd

conn=sqlite3.connect('xrv.db',detect_types=sqlite3.PARSE_DECLTYPES |
                                           sqlite3.PARSE_COLNAMES)

cur=conn.cursor()


cur.execute("""CREATE TABLE IF NOT EXISTS Data(
   datetime TIMESTAMP,
   gantry INT,
   energy FLOAT,
   angle FLOAT,
   x FLOAT,
   y FLOAT,
   z FLOAT,
   dx FLOAT,
   dy FLOAT,
   dz FLOAT,
   delta FLOAT,
   PRIMARY KEY (datetime,gantry,energy,angle));""")

conn.commit()


# Upload analyzed data

df = pd.read_csv("data_all.csv").iloc[:,1:][['datetime','gantry','energy','angle','x','y','z','dx','dy','dz','delta']]

all_data=[]
for i,d in df.iterrows():
    xx = tuple([str(i) for i in list(d)])
    all_data.append(xx)

cur.executemany("INSERT INTO Data VALUES(?,?,?,?,?,?,?,?,?,?,?);", all_data)
conn.commit()

conn.close()
